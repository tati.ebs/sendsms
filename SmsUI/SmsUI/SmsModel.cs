﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmsUI
{
   public class SmsModel
    {
        public SmsModel(int id, string numberFrom, string numberTo, string message, string dateCreated)
        {
            this.id = id;
            this.numberFrom = numberFrom;
            this.numberTo = numberTo;
            this.message = message;
            this.dateCreated = dateCreated;
        }

        public int id { get; set; }
            public string numberFrom { get; set; }
            public string numberTo { get; set; }
            public string message { get; set; }
            public string dateCreated { get; set; }
            public List<SmsModel> items { get; set; }

    }
}
