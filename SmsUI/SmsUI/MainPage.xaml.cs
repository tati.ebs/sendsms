﻿using RestSharp;

using System.Collections.Generic;

using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Microsoft.Toolkit.Uwp.UI.Controls;
using System;
using System.Data;
using System.Collections.ObjectModel;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace SmsUI
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
   
        public MainPage()
        {
            this.InitializeComponent();


            showList();






        }

        public IRestResponse listSms()
        {
            var restClient = new RestClient();

            restClient.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;

            RestRequest restRequest = new RestRequest("https://localhost:44372/message", Method.GET);
            IRestResponse restResponse = restClient.Execute(restRequest);

            var content = restResponse.Content;
            return restResponse;
        }

        public void showList()
        {
            var result = listSms().Content;
            var tr = JsonConvert.DeserializeObject(result.ToString());
            var tr1 = JsonConvert.DeserializeObject(tr.ToString());
            List<SmsModel> result1 = JsonConvert.DeserializeObject<List<SmsModel>>(tr1.ToString());           

            ListView gvSms = new ListView();
            for (int index = 0; index < result1.Count; index++)
            {               
                gvSms.Items.Add(String.Concat("ID: ", result1[index].id, ", numberFrom: ", result1[index].numberFrom, ", numberTo: ", result1[index].numberTo, ", message: ", result1[index].message, ", dateCreated: ", result1[index].dateCreated));
            }
            smsPanel.Children.Add(gvSms);

        }

        public IRestResponse sendSms()
        {
            var restClient = new RestClient();

            restClient.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;

            RestRequest restRequest = new RestRequest("https://localhost:44372/message", Method.POST);
            restRequest.AddJsonBody(new
            {
                numberTo = txtPhone.Text,
                numberFrom = "+14157262992",
                message = txtMessage.Text
            });

            IRestResponse restResponse = restClient.Execute(restRequest);

            var content = restResponse.Content;
            return restResponse;

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            
            var result = sendSms().Content;
            var tr = JsonConvert.DeserializeObject(result.ToString());
            var tr1 = JsonConvert.DeserializeObject(tr.ToString());


            List <SmsModel> result1 = JsonConvert.DeserializeObject<List<SmsModel>>(tr1.ToString());
            string showText = "";


            //var collection = new ObservableCollection<object>();

            for (int index = 0; index < result1.Count; index++)
            {
                showText = String.Concat("SMS Sent: ","ID: ", result1[index].id, ", numberFrom: ", result1[index].numberFrom, ", numberTo: ", result1[index].numberTo, ", message: ", result1[index].message, ", dateCreated: ", result1[index].dateCreated);
               
            }

            
            textBlock.Text = showText;
           



        }

        private void Clean_Click(object sender, RoutedEventArgs e)
        {
            txtPhone.Text = "";
            txtMessage.Text = "";
            textBlock.Text = "";
        }
    }
}
