﻿

using MessageModel;
using System.Threading.Tasks;
using Twilio.Rest.Api.V2010.Account;

namespace SendSmsService
{
    public interface ISendService
    {
        Task<MessageResource> sendSMS(SmsModel smsMessage);
    }
}
