﻿using MessageModel;
using System;
using System.Linq;
using System.Threading.Tasks;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;

namespace SendSmsService
{
    public class SendService : ISendService 
    {
        public async Task<MessageResource> sendSMS(SmsModel smsMessage)
        {
            //Twilio.Types.PhoneNumber("+14157262992")
            var messageBody = MessageBody(smsMessage);
           

            
                var message = await MessageResource.CreateAsync(
                    from: new PhoneNumber(smsMessage.numberFrom),
                        to: new PhoneNumber(smsMessage.numberTo),
                        body: messageBody);
                return message;
            

         

            

          
        }


        private string MessageBody(SmsModel smsMessage)
        {
            return $"{smsMessage.message} ";
        }
    }
}
