﻿using MessageModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace DataConnection
{
    public class ConnectionClass
    {
        SqlConnection con = new SqlConnection();

        public SqlConnection OpenConnection()
        {
            string connectionString = "Data Source = (localdb)\\MSSQLLocalDB; Initial Catalog = SendSms; Integrated Security = True; Connect Timeout = 30; Encrypt = False; TrustServerCertificate = False; ApplicationIntent = ReadWrite; MultiSubnetFailover = False";


            try

            {
                // open connection
                con = new SqlConnection(connectionString);
                con.Open();
                return con;
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
        public SqlConnection CloseConnection()
        {

            try
            {
                //close connection
                con.Close();
                return con;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public object AddMessage(SmsModel message)
        {
            try
            {
                object resultado;
                ConnectionClass cn = new ConnectionClass();
                              
                string sql = $"Insert Into Message (numberFrom, numberTo, message, dateCreated)  " +
                    $"Values ('{message.numberFrom }', '{message.numberTo}','{message.message }',getdate()); " + "SELECT CAST(scope_identity() AS int)"; ;

                using (SqlCommand cmd = new SqlCommand(sql, cn.OpenConnection()))
                {
                    cmd.CommandType = CommandType.Text;
                    resultado = cmd.ExecuteScalar();
                    cn.CloseConnection();
                }

              
                return resultado;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int AddResponse(ResponseModel response)
        {
            try
            {
                int resultado;
                ConnectionClass cn = new ConnectionClass();

                string sql = $"Insert Into Response (messageId, confirmationCode, dateSent) " +
                    $"Values ('{response.messageId }', '{response.confirmationCode}',getdate())";

                using (SqlCommand cmd = new SqlCommand(sql, cn.OpenConnection()))
                {
                    cmd.CommandType = CommandType.Text;
                    resultado = cmd.ExecuteNonQuery();
                    cn.CloseConnection();
                }


               
                return resultado;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public List<SmsModel> list()
        {
            try
            {
                List<SmsModel> smsList = new List<SmsModel>();

                SmsModel _sms = null;

                ConnectionClass cn = new ConnectionClass();
                string sql = $"Select top 10 * From Message";
                SqlCommand command = new SqlCommand(sql, cn.OpenConnection());

                using (SqlDataReader dataReader = command.ExecuteReader())
                {
                    while (dataReader.Read())
                    {
                        _sms = new SmsModel();
                        _sms.id = Convert.ToInt32(dataReader["Id"]);
                        _sms.numberFrom = Convert.ToString(dataReader["numberFrom"]);
                        _sms.numberTo = Convert.ToString(dataReader["numberTo"]);
                        DateTime fecha = Convert.ToDateTime(dataReader["dateCreated"]);
                        string cadena = fecha.ToString("dd/MM/yyyy");
                        _sms.dateCreated = cadena;
                        _sms.message = Convert.ToString(dataReader["message"]);

                        smsList.Add(_sms);
                    }
                }
                cn.CloseConnection();

                return smsList;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ResponseModel> Responselist(string ids)
        {
            try
            {
                List<ResponseModel> Responselist = new List<ResponseModel>();

                ResponseModel _response = null;

                ConnectionClass cn = new ConnectionClass();
                string sql = $"Select numberFrom,numberTo,dateCreated,message,b.* From Message a join Response b on a.Id=b.messageId " +
                 $"where a.Id in ('{ids.Trim(',')}')";
                SqlCommand command = new SqlCommand(sql, cn.OpenConnection());

                using (SqlDataReader dataReader = command.ExecuteReader())
                {
                    while (dataReader.Read())
                    {
                        _response = new ResponseModel();
                        _response.id = Convert.ToInt32(dataReader["Id"]);
                        _response.messageId  = Convert.ToInt32(dataReader["messageId"]);
                        _response.confirmationCode = Convert.ToString(dataReader["confirmationCode"]);
                        _response.errorMessage = Convert.ToString(dataReader["errorMessage"]);
                        DateTime fecha1 = Convert.ToDateTime(dataReader["dateSent"]);
                        string cadena1 = fecha1.ToString("dd/MM/yyyy");
                        _response.dateSent = cadena1;
                        _response.numberFrom = Convert.ToString(dataReader["numberFrom"]);
                        _response.numberTo = Convert.ToString(dataReader["numberTo"]);
                        DateTime fecha = Convert.ToDateTime(dataReader["dateCreated"]);
                        string cadena = fecha.ToString("dd/MM/yyyy");
                        _response.dateCreated = cadena;
                        _response.message = Convert.ToString(dataReader["message"]);
                    

                        Responselist.Add(_response);
                    }
                }
                cn.CloseConnection();

                return Responselist;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
