﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MessageModel
{
   public class ResponseModel
    {
        public int id { get; set; }
        public int messageId { get; set; }       
        public string confirmationCode { get; set; }
        public string dateSent { get; set; }
        public string errorMessage { get; set; }
        public string numberFrom { get; set; }
        public string numberTo { get; set; }
        public string message { get; set; }
        public string dateCreated { get; set; }
        public List<ResponseModel> items { get; set; }
    }
}
