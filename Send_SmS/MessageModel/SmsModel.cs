﻿using System;
using System.Collections.Generic;

namespace MessageModel
{
    public class SmsModel
    {
        public int id { get; set; }
        public string numberFrom { get; set; }
        public string numberTo { get; set; }
        public string message { get; set; }
        public string dateCreated { get; set; }
        public List<SmsModel> items { get; set; }

    }
}
