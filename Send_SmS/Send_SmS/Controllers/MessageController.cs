﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MessageModel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SendSmsService;
using Microsoft.Extensions.Configuration;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using DataConnection;
using System.Text.Json;
using Newtonsoft.Json;

namespace Send_SmS.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class MessageController : ControllerBase
    {

        private readonly ISendService _smsService;

        public MessageController()
        {
            this._smsService = new SendService();
        }
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Get()
        {
            ConnectionClass cn = new ConnectionClass();
            cn = new ConnectionClass();
            SmsModel items = new SmsModel();
            var output = JsonConvert.SerializeObject(cn.list());

            return Ok(output.ToString()); 

        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Post([FromBody]SmsModel model)
        {
            string[] _numberTo = model.numberTo.Split(',');

            SmsModel _model;
            ConnectionClass cn = new ConnectionClass();
            ResponseModel _response;
           
            string ids = "";
            int res = 0;

            cn = new ConnectionClass();

            foreach (var num in _numberTo)
            {
                _model = new SmsModel();
                _model.numberTo = num;
                _model.numberFrom = model.numberFrom;
                _model.message = model.message;

              var result = await _smsService.sendSMS(_model);

              
                object id =  cn.AddMessage(_model);

                ids = ids + id.ToString() + ",";

                _response = new ResponseModel();

                _response.confirmationCode = result.Sid;
                _response.messageId = (Int32)id;
                _response.dateSent = Convert.ToString(result.DateSent);

                

                

                if ((Int32)id != 0)
                {
                   res= cn.AddResponse(_response);                  

                }


            }

           

            if (res != 0)
            {
               
                ResponseModel items = new ResponseModel();
                string output = JsonConvert.SerializeObject(cn.Responselist(ids));
           
                return Ok(output);
            }
                                

            else
            { return null; }


        }



      

    }
}